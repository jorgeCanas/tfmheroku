# -*- coding: utf-8 -*-

import re

from django.core.management.base import BaseCommand
from pueblos.common.util.utilities import clean_accents
from pueblos.models import PueblosNoticiasTest, PueblosNoticiasTestCategorias


class Command(BaseCommand):
    """Use example:
     python manage.py test_context_set_matrix --test_file prueba_words_category.csv --output_file confusion_matrix"""
    help = 'Realiza el test del contexto creado y genera matrix de confusion'

    def add_arguments(self, parser):
        parser.add_argument('--number_of_notices', dest='number_of_notices', nargs='?', default='3',
                            help='Numero de noticias de test a coger')
        parser.add_argument('--output_file', dest='output_file', nargs='?', default='',
                            help='Nombre del fichero donde se guardara la matriz de confusion')

    def handle(self, *args, **options):
        output_file = options['output_file']
        savefile = 'files/randon_categorized_notices.txt'
        if output_file:
            if 'files_batch/' in output_file:
                savefile = output_file
            elif 'files/' not in output_file:
                savefile = 'files/' + output_file

            if '.txt' not in savefile:
                savefile = output_file + '.txt'
        with open(savefile, 'w') as f:
            number_of_notices = options['number_of_notices']
            text = ""

            notices = PueblosNoticiasTest.objects.order_by('?')[:number_of_notices]
            for notice in notices:
                text += 'Id noticia ' + str(notice.noticia.id) + ' Id noticia test ' + str(notice.id) + '\n'
                text += notice.dscuerpo + '\n\n'
                text += 'Categorias guardadas\n'
                categories = notice.get_categorias_string()
                for category in categories:
                    category = 'ETIQUETA_' + re.sub(ur'[ ]', '_', category.lower())
                    category = clean_accents(category)
                    text += category + ', '
                text += '\nCategorias sugeridas\n'
                suggested_categories = PueblosNoticiasTestCategorias.objects.filter(noticia_test=notice)
                if len(suggested_categories) == 0:
                    text += '[]'
                else:
                    for suggested_category in suggested_categories:
                        text += suggested_category.categorias_sugeridas + ', '
                text += '\n\n\n'
            text = text.encode('utf-8')
            f.write(text)
            return self.stdout.write('Finished')
