#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from . import views

app_name = 'pueblos'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<noticia_200_id>[0-9]+)/noticia200/$', views.noticia200, name='noticia200'),
    url(r'^(?P<noticia_200_id>[0-9]+)/delete_words/$', views.delete_words, name='delete_words'),
    url(r'^listaNoticias200/$', views.ListaNoticias200View.as_view(), name='listaNoticias200'),
    url(r'^(?P<noticia_id>[0-9]+)/show_notice_categories/$', views.show_notice_categories,
        name='show_notice_categories'),
    url(r'^noticesContext/$', views.notices_context_view, name='notices_context'),
    # url(r'^noticesContextStep/$', views.NoticesContextStepView, name='notices_context_step'),
    url(r'^noticesContextStep/$', TemplateView.as_view(template_name="pueblos/noticesContextStep.html"),
        name='notices_context_step'),
    url(r'^set_context_options_run/$', views.set_context_options_run, name='set_context_options_run'),
    # url(r'^processWord/setIndividualContext/$', views.context_options_process, name='context_options_process'),
    url(r'(?P<option>[a-zA-Z]+)/setIndividualContext/$', views.context_options_router, name='context_options_router'),
    url(r'(?P<page>[0-9]+)/syntactic_dictionary/(?P<filter>\w+)$', views.syntactic_dictionary,
        name='syntactic_dictionary'),
    url(r'^search_word/$', views.search_word, name="search_word"),
    url(r'^save_word/$', views.save_word, name="save_word"),
    url(r'^noticeSuggestTags/$', views.notice_suggest_tags, name='notice_suggest_tags'),
    url(r'^(?P<test_case_id>[0-9]+)/checkStatus/$', views.check_status, name='check_status'),
    url(r'^singularize/$', views.singularize, name='singularize'),
    # url(r'^test_task/$', views.test_task_view, name='test_task'),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
